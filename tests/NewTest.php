<?php

declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class NewTest extends TestCase
{
    public function testTestsAreRunning(): void
    {
        $value = rand(1, 1000);

        static::assertGreaterThan(1, $value);
    }
}
