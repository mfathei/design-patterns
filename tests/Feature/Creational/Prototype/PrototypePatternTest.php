<?php

declare(strict_types=1);

namespace Tests\Feature\Creational\Prototype;

use App\Creational\Prototype\CloneFactory;
use App\Creational\Prototype\Sheep;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class PrototypePatternTest extends TestCase
{
    public function testProtoTypePattern(): void
    {
        $factory = new CloneFactory();
        $sally = new Sheep();
        $clonedSheep = $factory->getClone($sally);

        static::assertInstanceOf(Sheep::class, $sally);
        static::assertInstanceOf(Sheep::class, $clonedSheep);
    }
}
