<?php

declare(strict_types=1);

namespace Tests\Feature\Creational\Factory;

use App\Creational\Factory\BigUFOEnemyShip;
use App\Creational\Factory\EnemyShipFactory;
use App\Creational\Factory\RocketEnemyShip;
use App\Creational\Factory\UFOEnemyShip;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class FactoryTest extends TestCase
{
    public function testItCreatesTheRightType(): void
    {
        $type = 'U';
        $bigType = 'B';
        $rocketType = 'R';

        $invalidShip = EnemyShipFactory::makeEnemyShip('invalid');
        $ship = EnemyShipFactory::makeEnemyShip($type);
        $bigShip = EnemyShipFactory::makeEnemyShip($bigType);
        $rocketShip = EnemyShipFactory::makeEnemyShip($rocketType);

        static::assertNull($invalidShip);
        static::assertInstanceOf(UFOEnemyShip::class, $ship);
        static::assertInstanceOf(BigUFOEnemyShip::class, $bigShip);
        static::assertInstanceOf(RocketEnemyShip::class, $rocketShip);
    }
}
