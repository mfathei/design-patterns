<?php

declare(strict_types=1);

namespace Tests\Feature\Creational\AbstractFactory;

use App\Creational\AbstractFactory\UFOEnemyShip;
use App\Creational\AbstractFactory\UFOEnemyShipBuilding;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class AbstractFactoryTest extends TestCase
{
    public function testItCreatesTheRightType(): void
    {
        $makeUFOs = new UFOEnemyShipBuilding();

        $UFOEnemyShip = $makeUFOs->orderTheShip('UFO');

        static::assertInstanceOf(UFOEnemyShip::class, $UFOEnemyShip);
        static::assertSame('The UFO Grunt ship has a top speed of 100 mph and and attack power of 20 damage', $UFOEnemyShip->__toString());
    }
}
