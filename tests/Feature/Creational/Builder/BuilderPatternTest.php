<?php

declare(strict_types=1);

namespace Tests\Feature\Creational\Builder;

use App\Creational\Builder\OldRobotBuilder;
use App\Creational\Builder\RobotEngineer;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class BuilderPatternTest extends TestCase
{
    public function testItCreatesOldRobot(): void
    {
        $oldRobotBuilder = new OldRobotBuilder();
        $robotEngineer = new RobotEngineer($oldRobotBuilder);

        $robotEngineer->makeRobot();
        $robot = $robotEngineer->getRobot();

        static::assertSame('Tin head', $robot->getRobotHead());
        static::assertSame('Tin torso', $robot->getRobotTorso());
        static::assertSame('Blowtorch arms', $robot->getRobotArms());
        static::assertSame('Roller Skates', $robot->getRobotLegs());
    }
}
