<?php

declare(strict_types=1);

namespace Tests\Feature\Creational\Singleton;

use App\Creational\Singleton\EnemyShip;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class TestSingletonPattern extends TestCase
{
    public function testItGetsTheSameInstance(): void
    {
        $instanceOne = EnemyShip::getInstance();
        $instanceTwo = EnemyShip::getInstance();

        static::assertSame($instanceOne, $instanceTwo);
    }
}
