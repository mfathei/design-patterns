<?php

declare(strict_types=1);

namespace Tests\Feature\Structural\Decorator;

use App\Structural\Decorator\Book;
use App\Structural\Decorator\BookTitleDecorator;
use App\Structural\Decorator\BookTitleExclaimDecorator;
use App\Structural\Decorator\BookTitleStarDecorator;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class DecoratorPatternTest extends TestCase
{
    public function testItWorks(): void
    {
        $book = new Book('Gamma, Helm, Johnson, and Vlissides', 'Design Patterns');

        $decorator = new BookTitleDecorator($book);
        $starDecorator = new BookTitleStarDecorator($decorator);
        $exclaimDecorator = new BookTitleExclaimDecorator($decorator);

        static::assertSame('Gamma, Helm, Johnson, and Vlissides', $decorator->showTitle());

        $exclaimDecorator->exclaimTitle();
        $exclaimDecorator->exclaimTitle();
        static::assertSame('!!Gamma, Helm, Johnson, and Vlissides!!', $decorator->showTitle());

        $starDecorator->starTitle();
        static::assertSame('!!Gamma,*Helm,*Johnson,*and*Vlissides!!', $decorator->showTitle());

        $decorator->resetTitle();
        static::assertSame('Gamma, Helm, Johnson, and Vlissides', $decorator->showTitle());
    }
}
