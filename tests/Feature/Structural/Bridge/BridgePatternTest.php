<?php

declare(strict_types=1);

namespace Tests\Feature\Structural\Bridge;

use App\Structural\Bridge\RemoteButton;
use App\Structural\Bridge\TVDevice;
use App\Structural\Bridge\TVRemoteMute;
use App\Structural\Bridge\TVRemotePause;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class BridgePatternTest extends TestCase
{
    public function testIfItWorks(): void
    {
        $tvOne = new TVRemoteMute(new TVDevice(1, 200));
        $tvTwo = new TVRemotePause(new TVDevice(1, 200));

        $tvOne->buttonFivePressed();
        $tvOne->buttonSixPressed();
        $tvOne->buttonNinePressed();

        $tvTwo->buttonFivePressed();
        $tvTwo->buttonSixPressed();
        $tvTwo->buttonSixPressed();
        $tvTwo->buttonSixPressed();
        $tvTwo->buttonSixPressed();
        $tvTwo->buttonNinePressed();

        static::assertInstanceOf(RemoteButton::class, $tvOne);
        static::assertInstanceOf(RemoteButton::class, $tvTwo);
    }
}
