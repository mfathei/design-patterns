<?php

declare(strict_types=1);

namespace Tests\Feature\Structural\Adapter;

use App\Structural\Adapter\EnemyAttacker;
use App\Structural\Adapter\EnemyRobot;
use App\Structural\Adapter\EnemyRobotAdapter;
use App\Structural\Adapter\EnemyTank;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class AdapterPatternTest extends TestCase
{
    public function testItWorks(): void
    {
        $rx7Tank = new EnemyTank();
        $fredTheRobot = new EnemyRobot();
        $robotAdapter = new EnemyRobotAdapter($fredTheRobot);

        echo 'Enemy Tank:';
        $rx7Tank->fireWeapon();
        $rx7Tank->driveForward();
        $rx7Tank->assignDriver('Frank');

        echo 'Enemy Robot:';
        $fredTheRobot->smashWithHands();
        $fredTheRobot->moveForward();
        $fredTheRobot->reactToHuman('Paul');

        echo 'Enemy Robot Adapter:';
        $robotAdapter->fireWeapon();
        $robotAdapter->driveForward();
        $robotAdapter->assignDriver('Mark');

        static::assertInstanceOf(EnemyAttacker::class, $robotAdapter);
    }
}
