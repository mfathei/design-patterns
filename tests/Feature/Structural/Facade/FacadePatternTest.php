<?php

declare(strict_types=1);

namespace Tests\Feature\Structural\Facade;

use App\Structural\Facade\Book;
use App\Structural\Facade\CaseReverseFacade;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class FacadePatternTest extends TestCase
{
    public function testItWorks(): void
    {
        $book = new Book('Design Patterns', 'Gamma, Helm, Johnson, and Vlissides');
        static::assertSame('Design Patterns', $book->getTitle());

        $bookTitleReversed = CaseReverseFacade::reverseStringCase($book->getTitle());

        static::assertSame('dESIGN pATTERNS', $bookTitleReversed);
    }
}
