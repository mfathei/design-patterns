<?php

declare(strict_types=1);

namespace Tests\Feature\Structural\Composite;

use App\Structural\Composite\OneBook;
use App\Structural\Composite\SeveralBooks;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class CompositePatternTest extends TestCase
{
    public function testItWorks(): void
    {
        $firstBook = new OneBook('Core PHP Programming, Third Edition', 'Atkinson and Suraski');
        static::assertSame('Core PHP Programming, Third Edition by Atkinson and Suraski', $firstBook->getBookInfo());

        $secondBook = new OneBook('PHP Bible', 'Converse and Park');
        static::assertSame('PHP Bible by Converse and Park', $secondBook->getBookInfo());

        $thirdBook = new OneBook('Design Patterns', 'Gamma, Helm, Johnson, and Vlissides');
        static::assertSame('Design Patterns by Gamma, Helm, Johnson, and Vlissides', $thirdBook->getBookInfo());

        $books = new SeveralBooks();

        $booksCount = $books->addBook($firstBook);
        static::assertSame('Core PHP Programming, Third Edition by Atkinson and Suraski', $books->getBookInfo($booksCount));

        $booksCount = $books->addBook($secondBook);
        static::assertSame('PHP Bible by Converse and Park', $books->getBookInfo($booksCount));

        $booksCount = $books->addBook($thirdBook);
        static::assertSame('Design Patterns by Gamma, Helm, Johnson, and Vlissides', $books->getBookInfo($booksCount));

        $booksCount = $books->removeBook($firstBook);
        static::assertSame(2, $books->getBookCount());
        static::assertSame('PHP Bible by Converse and Park', $books->getBookInfo());
        static::assertSame('Design Patterns by Gamma, Helm, Johnson, and Vlissides', $books->getBookInfo(2));
    }
}
