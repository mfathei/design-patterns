<?php

declare(strict_types=1);

namespace App\Creational\AbstractFactory;

class UFOEnemyShip extends EnemyShip
{
    protected EnemyShipFactory $enemyShipFactory;

    public function __construct(EnemyShipFactory $enemyShipFactory)
    {
        parent::__construct();

        $this->enemyShipFactory = $enemyShipFactory;
    }

    public function makeShip(): void
    {
        echo 'Making enemy ship '.$this->getName();

        $this->weapon = $this->enemyShipFactory->addESGun();
        $this->engine = $this->enemyShipFactory->addESEngine();
    }
}
