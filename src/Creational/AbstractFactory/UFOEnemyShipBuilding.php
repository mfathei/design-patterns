<?php

declare(strict_types=1);

namespace App\Creational\AbstractFactory;

class UFOEnemyShipBuilding extends EnemyShipBuilding
{
    protected function makeEnemyShip(string $typeOfShip): ?EnemyShip
    {
        $theEnemyShip = null;

        if ('UFO' === $typeOfShip) {
            $shipPartsFactory = new UFOEnemyShipFactory();
            $theEnemyShip = new UFOEnemyShip($shipPartsFactory);
            $theEnemyShip->setName('UFO Grunt ship');
        }

        return $theEnemyShip;
    }
}
