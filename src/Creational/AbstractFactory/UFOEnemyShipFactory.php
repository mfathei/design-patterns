<?php

declare(strict_types=1);

namespace App\Creational\AbstractFactory;

class UFOEnemyShipFactory implements EnemyShipFactory
{
    public function addESGun(): ESWeapon
    {
        return new ESUFOGun();
    }

    public function addESEngine(): ESEngine
    {
        return new ESUFOEngine();
    }
}
