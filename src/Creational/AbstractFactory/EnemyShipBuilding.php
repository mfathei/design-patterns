<?php

declare(strict_types=1);

namespace App\Creational\AbstractFactory;

abstract class EnemyShipBuilding
{
    public function orderTheShip(string $typeOfShip): ?EnemyShip
    {
        $theEnemyShip = $this->makeEnemyShip($typeOfShip);

        if ($theEnemyShip) {
            $theEnemyShip->makeShip();
        }

        return $theEnemyShip;
    }

    abstract protected function makeEnemyShip(string $typeOfShip): ?EnemyShip;
}
