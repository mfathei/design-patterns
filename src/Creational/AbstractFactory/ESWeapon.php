<?php

declare(strict_types=1);

namespace App\Creational\AbstractFactory;

interface ESWeapon
{
    public function __toString(): string;
}
