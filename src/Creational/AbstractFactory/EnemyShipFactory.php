<?php

declare(strict_types=1);

namespace App\Creational\AbstractFactory;

interface EnemyShipFactory
{
    public function addESGun(): ESWeapon;

    public function addESEngine(): ESEngine;
}
