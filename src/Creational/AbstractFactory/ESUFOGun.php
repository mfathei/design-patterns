<?php

declare(strict_types=1);

namespace App\Creational\AbstractFactory;

class ESUFOGun implements ESWeapon
{
    public function __toString(): string
    {
        return '20 damage';
    }
}
