<?php

declare(strict_types=1);

namespace App\Creational\AbstractFactory;

interface ESEngine
{
    public function __toString(): string;
}
