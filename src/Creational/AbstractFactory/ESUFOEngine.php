<?php

declare(strict_types=1);

namespace App\Creational\AbstractFactory;

class ESUFOEngine implements ESEngine
{
    public function __toString(): string
    {
        return '100 mph';
    }
}
