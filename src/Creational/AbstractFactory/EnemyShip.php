<?php

declare(strict_types=1);

namespace App\Creational\AbstractFactory;

abstract class EnemyShip
{
    protected string $name;
    protected ESWeapon $weapon;
    protected ESEngine $engine;

    public function __construct(string $name = '', ESWeapon $weapon = null, ESEngine $engine = null)
    {
        $this->name = $name;
        $this->weapon = $weapon ?? new ESUFOGun();
        $this->engine = $engine ?? new ESUFOEngine();
    }

    public function __toString(): string
    {
        return 'The '.$this->name.' has a top speed of '.$this->engine.' and and attack power of '.$this->weapon;
    }

    abstract public function makeShip(): void;

    /**
     * @param string $name
     *
     * @return \App\Creational\AbstractFactory\EnemyShip
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
