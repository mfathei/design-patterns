<?php

declare(strict_types=1);

namespace App\Creational\Builder;

use App\Creational\Builder\Concerns\HasArms;
use App\Creational\Builder\Concerns\HasHead;
use App\Creational\Builder\Concerns\HasLegs;
use App\Creational\Builder\Concerns\HasTorso;

class Robot implements HasHead, HasTorso, HasArms, HasLegs
{
    protected string $head;
    protected string $torso;
    protected string $arms;
    protected string $legs;

    public function __construct()
    {
        $this->head = '';
        $this->torso = '';
        $this->arms = '';
        $this->legs = '';
    }

    public function getRobotHead(): string
    {
        return $this->head;
    }

    public function setRobotHead(string $head): self
    {
        $this->head = $head;

        return $this;
    }

    public function getRobotTorso(): string
    {
        return $this->torso;
    }

    public function setRobotTorso(string $torso): self
    {
        $this->torso = $torso;

        return $this;
    }

    public function getRobotArms(): string
    {
        return $this->arms;
    }

    public function setRobotArms(string $arms): self
    {
        $this->arms = $arms;

        return $this;
    }

    public function getRobotLegs(): string
    {
        return $this->legs;
    }

    public function setRobotLegs(string $legs): self
    {
        $this->legs = $legs;

        return $this;
    }
}
