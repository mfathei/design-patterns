<?php

declare(strict_types=1);

namespace App\Creational\Builder\Concerns;

interface HasLegs
{
    public function getRobotLegs(): string;

    public function setRobotLegs(string $legs): self;
}
