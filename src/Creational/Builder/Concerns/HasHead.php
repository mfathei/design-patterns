<?php

declare(strict_types=1);

namespace App\Creational\Builder\Concerns;

interface HasHead
{
    public function getRobotHead(): string;

    public function setRobotHead(string $head): self;
}
