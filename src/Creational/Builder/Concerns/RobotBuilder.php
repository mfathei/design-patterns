<?php

declare(strict_types=1);

namespace App\Creational\Builder\Concerns;

use App\Creational\Builder\Robot;

interface RobotBuilder
{
    public function buildRobotHead(): void;

    public function buildRobotTorso(): void;

    public function buildRobotArms(): void;

    public function buildRobotLegs(): void;

    public function getRobot(): Robot;
}
