<?php

declare(strict_types=1);

namespace App\Creational\Builder\Concerns;

interface HasArms
{
    public function getRobotArms(): string;

    public function setRobotArms(string $arms): self;
}
