<?php

declare(strict_types=1);

namespace App\Creational\Builder\Concerns;

interface HasTorso
{
    public function getRobotTorso(): string;

    public function setRobotTorso(string $torso): self;
}
