<?php

declare(strict_types=1);

namespace App\Creational\Builder;

use App\Creational\Builder\Concerns\RobotBuilder;

class OldRobotBuilder implements RobotBuilder
{
    protected Robot $robot;

    public function __construct()
    {
        $this->robot = new Robot();
    }

    public function buildRobotHead(): void
    {
        $this->robot->setRobotHead('Tin head');
    }

    public function buildRobotTorso(): void
    {
        $this->robot->setRobotTorso('Tin torso');
    }

    public function buildRobotArms(): void
    {
        $this->robot->setRobotArms('Blowtorch arms');
    }

    public function buildRobotLegs(): void
    {
        $this->robot->setRobotLegs('Roller Skates');
    }

    public function getRobot(): Robot
    {
        return $this->robot;
    }
}
