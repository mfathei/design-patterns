<?php

declare(strict_types=1);

namespace App\Creational\Builder;

use App\Creational\Builder\Concerns\RobotBuilder;

class RobotEngineer
{
    protected RobotBuilder $robotBuilder;

    public function __construct(RobotBuilder $robotBuilder)
    {
        $this->robotBuilder = $robotBuilder;
    }

    public function getRobot(): Robot
    {
        return $this->robotBuilder->getRobot();
    }

    public function makeRobot(): void
    {
        $this->robotBuilder->buildRobotHead();
        $this->robotBuilder->buildRobotTorso();
        $this->robotBuilder->buildRobotArms();
        $this->robotBuilder->buildRobotLegs();
    }
}
