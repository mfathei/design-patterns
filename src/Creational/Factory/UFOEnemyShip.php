<?php

declare(strict_types=1);

namespace App\Creational\Factory;

class UFOEnemyShip extends EnemyShip
{
    public function __construct()
    {
        parent::__construct('UFO enemy ship', 20.0);
    }
}
