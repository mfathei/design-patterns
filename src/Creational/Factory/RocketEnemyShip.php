<?php

declare(strict_types=1);

namespace App\Creational\Factory;

class RocketEnemyShip extends EnemyShip
{
    public function __construct()
    {
        parent::__construct('Rocket enemy ship', 10.0);
    }
}
