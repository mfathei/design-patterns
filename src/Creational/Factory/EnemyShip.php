<?php

declare(strict_types=1);

namespace App\Creational\Factory;

abstract class EnemyShip
{
    protected string $name;
    protected float $amtDamage;

    public function __construct(string $name = '', float $damage = 0.0)
    {
        $this->name = $name;
        $this->amtDamage = $damage;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAmtDamage(): float
    {
        return $this->amtDamage;
    }

    public function setAmtDamage(float $amtDamage): self
    {
        $this->amtDamage = $amtDamage;

        return $this;
    }

    public function followHeroShip(): void
    {
        echo $this->getName().' is following the hero';
    }

    public function displayEnemyShip(): void
    {
        echo $this->getName().' is on the screen';
    }

    public function enemyShipShoots(): void
    {
        echo $this->getName().' attacks and does'.$this->getAmtDamage();
    }
}
