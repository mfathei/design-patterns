<?php

declare(strict_types=1);

namespace App\Creational\Factory;

class EnemyShipFactory
{
    public static function makeEnemyShip(string $type): ?EnemyShip
    {
        if ('U' === $type) {
            return new UFOEnemyShip();
        }

        if ('B' === $type) {
            return new BigUFOEnemyShip();
        }

        if ('R' === $type) {
            return new RocketEnemyShip();
        }

        return null;
    }
}
