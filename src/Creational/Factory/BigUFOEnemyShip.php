<?php

declare(strict_types=1);

namespace App\Creational\Factory;

class BigUFOEnemyShip extends EnemyShip
{
    public function __construct()
    {
        parent::__construct('Big UFO enemy ship', 40.0);
    }
}
