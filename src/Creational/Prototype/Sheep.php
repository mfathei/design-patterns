<?php

declare(strict_types=1);

namespace App\Creational\Prototype;

class Sheep implements Animal
{
    public function __construct()
    {
        echo 'Sheep is being made';
    }

    public function makeCopy(): self
    {
        return clone $this;
    }
}
