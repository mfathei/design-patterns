<?php

declare(strict_types=1);

namespace App\Creational\Prototype;

interface Animal
{
    public function makeCopy(): self;
}
