<?php

declare(strict_types=1);

namespace App\Creational\Prototype;

class CloneFactory
{
    public function getClone(Animal $animal): Animal
    {
        return $animal->makeCopy();
    }
}
