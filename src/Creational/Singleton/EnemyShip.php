<?php

declare(strict_types=1);

namespace App\Creational\Singleton;

class EnemyShip
{
    private static ?EnemyShip $instance;

    private function __construct()
    {
    }

    /**
     * @return \App\Creational\Singleton\EnemyShip
     */
    public static function getInstance(): self
    {
        if (! static::$instance) {
            static::$instance = new self();
        }

        return self::$instance;
    }
}
