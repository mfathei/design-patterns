<?php

declare(strict_types=1);

if (! function_exists('writeln')) {
    function writeln(string $line): void
    {
        echo $line.PHP_EOL;
    }
}
