<?php

declare(strict_types=1);

namespace App\Structural\Adapter;

class EnemyRobot
{
    /**
     * @throws \Exception
     */
    public function smashWithHands(): void
    {
        echo 'Enemy robot causes '.random_int(1, 10).' damage with its hands';
    }

    /**
     * @throws \Exception
     */
    public function moveForward(): void
    {
        echo 'Enemy robot moves '.random_int(1, 5).' spaces';
    }

    public function reactToHuman(string $driverName): void
    {
        echo 'Enemy robot tramps on '.$driverName;
    }
}
