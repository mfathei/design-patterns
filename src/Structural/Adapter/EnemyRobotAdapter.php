<?php

declare(strict_types=1);

namespace App\Structural\Adapter;

class EnemyRobotAdapter implements EnemyAttacker
{
    protected EnemyRobot $robot;

    public function __construct(EnemyRobot $robot)
    {
        $this->robot = $robot;
    }

    /**
     * @throws \Exception
     */
    public function fireWeapon(): void
    {
        $this->robot->smashWithHands();
    }

    /**
     * @throws \Exception
     */
    public function driveForward(): void
    {
        $this->robot->moveForward();
    }

    public function assignDriver(string $driverName): void
    {
        $this->robot->reactToHuman($driverName);
    }
}
