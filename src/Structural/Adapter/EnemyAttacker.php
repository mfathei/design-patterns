<?php

declare(strict_types=1);

namespace App\Structural\Adapter;

interface EnemyAttacker
{
    public function fireWeapon(): void;

    public function driveForward(): void;

    public function assignDriver(string $driverName): void;
}
