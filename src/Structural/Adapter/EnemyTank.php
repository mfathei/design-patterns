<?php

declare(strict_types=1);

namespace App\Structural\Adapter;

class EnemyTank implements EnemyAttacker
{
    /**
     * @throws \Exception
     */
    public function fireWeapon(): void
    {
        echo 'Enemy tank does '.random_int(1, 10).' damage';
    }

    /**
     * @throws \Exception
     */
    public function driveForward(): void
    {
        echo 'Enemy tank moves '.random_int(1, 5).' spaces';
    }

    public function assignDriver(string $driverName): void
    {
        echo $driverName.' is driving the tank';
    }
}
