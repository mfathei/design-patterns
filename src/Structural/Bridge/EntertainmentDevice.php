<?php

declare(strict_types=1);

namespace App\Structural\Bridge;

abstract class EntertainmentDevice
{
    protected int $deviceState;
    protected int $maxSetting;
    protected int $volumeLevel;

    public function __construct(int $deviceState = 0, int $maxSetting = 10, int $volumeLevel = 0)
    {
        $this->deviceState = $deviceState;
        $this->maxSetting = $maxSetting;
        $this->volumeLevel = 0;
    }

    abstract public function buttonFivePressed(): void;

    abstract public function buttonSixPressed(): void;

    public function deviceFeedback(): void
    {
        if ($this->deviceState > $this->maxSetting || $this->deviceState < 0) {
            $this->deviceState = 0;
        }

        echo 'On: '.$this->deviceState;
    }

    public function buttonSevenPressed(): void
    {
        ++$this->volumeLevel;

        echo 'Volume at: '.$this->volumeLevel;
    }

    public function buttonEightPressed(): void
    {
        --$this->volumeLevel;

        echo 'Volume at: '.$this->volumeLevel;
    }
}
