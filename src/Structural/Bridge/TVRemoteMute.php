<?php

declare(strict_types=1);

namespace App\Structural\Bridge;

class TVRemoteMute extends RemoteButton
{
    public function buttonNinePressed(): void
    {
        echo 'TV was Muted';
    }
}
