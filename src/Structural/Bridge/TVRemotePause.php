<?php

declare(strict_types=1);

namespace App\Structural\Bridge;

class TVRemotePause extends RemoteButton
{
    public function buttonNinePressed(): void
    {
        echo 'TV was Paused';
    }
}
