<?php

declare(strict_types=1);

namespace App\Structural\Bridge;

class TVDevice extends EntertainmentDevice
{
    public function __construct(int $deviceState, int $maxSetting)
    {
        parent::__construct($deviceState, $maxSetting);
    }

    public function buttonFivePressed(): void
    {
        ++$this->deviceState;

        echo 'Channel up';
    }

    public function buttonSixPressed(): void
    {
        --$this->deviceState;

        echo 'Channel down';
    }
}
