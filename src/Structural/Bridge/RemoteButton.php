<?php

declare(strict_types=1);

namespace App\Structural\Bridge;

abstract class RemoteButton
{
    protected EntertainmentDevice $device;

    public function __construct(EntertainmentDevice $device)
    {
        $this->device = $device;
    }

    public function buttonFivePressed(): void
    {
        $this->device->buttonFivePressed();
    }

    public function buttonSixPressed(): void
    {
        $this->device->buttonSixPressed();
    }

    abstract public function buttonNinePressed(): void;
}
