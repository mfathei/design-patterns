<?php

declare(strict_types=1);

namespace App\Structural\Facade;

class ArrayStringFunctions
{
    public static function arrayToString(array $arrayIn): string
    {
        $stringOut = '';
        foreach ($arrayIn as $char) {
            $stringOut .= $char;
        }

        return $stringOut;
    }

    public static function stringToArray(string $text): array
    {
        return str_split($text);
    }
}
