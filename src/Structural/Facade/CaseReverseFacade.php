<?php

declare(strict_types=1);

namespace App\Structural\Facade;

class CaseReverseFacade
{
    public static function reverseStringCase(string $text): string
    {
        $arrayFromString = ArrayStringFunctions::stringToArray($text);
        $reversedArray = ArrayCaseReverse::reverseCase($arrayFromString);

        return ArrayStringFunctions::arrayToString($reversedArray);
    }
}
