<?php

declare(strict_types=1);

namespace App\Structural\Facade;

class ArrayCaseReverse
{
    private static $uppercaseArray = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    private static $lowercaseArray = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

    public static function reverseCase(array $arrayIn): array
    {
        $arrayOut = [];
        $count = \count($arrayIn);

        for ($x = 0; $x < $count; ++$x) {
            $key = array_search($arrayIn[$x], self::$lowercaseArray, true);
            if (false !== $key) {
                $arrayOut[$x] = self::$uppercaseArray[$key];
                continue;
            }

            $key = array_search($arrayIn[$x], self::$uppercaseArray, true);
            if (false !== $key) {
                $arrayOut[$x] = self::$lowercaseArray[$key];
                continue;
            }

            $arrayOut[$x] = $arrayIn[$x];
        }

        return $arrayOut;
    }
}
