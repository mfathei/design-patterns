<?php

declare(strict_types=1);

namespace App\Structural\Decorator;

class BookTitleDecorator
{
    /** @var Book */
    protected $book;
    /** @var string */
    protected $title;

    public function __construct(Book $book)
    {
        $this->book = $book;
        $this->resetTitle();
    }

    public function resetTitle(): void
    {
        $this->title = $this->book->getTitle();
    }

    public function showTitle()
    {
        return $this->title;
    }
}
