<?php

declare(strict_types=1);

namespace App\Structural\Decorator;

class BookTitleStarDecorator extends BookTitleDecorator
{
    /** @var BookTitleDecorator */
    private $btd;

    public function __construct(BookTitleDecorator $btd)
    {
        $this->btd = $btd;
    }

    public function starTitle()
    {
        $this->btd->title = str_replace(' ', '*', $this->btd->title);
    }
}
