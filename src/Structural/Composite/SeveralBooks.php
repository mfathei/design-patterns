<?php

declare(strict_types=1);

namespace App\Structural\Composite;

class SeveralBooks implements OnTheBookShelf
{
    /** @var OnTheBookShelf[] */
    private $books = [];
    /** @var int */
    private $booksCount;

    public function __construct()
    {
        $this->booksCount = 0;
    }

    public function getBookInfo(int $index = 1): string
    {
        if ($index <= $this->booksCount) {
            return ($this->books[$index])->getBookInfo();
        }

        return '';
    }

    public function getBookCount(): int
    {
        return $this->booksCount;
    }

    public function setBookCount(int $newCount): self
    {
        $this->booksCount = $newCount;

        return $this;
    }

    public function addBook(OnTheBookShelf $book): int
    {
        $this->setBookCount($this->getBookCount() + 1);
        $this->books[$this->getBookCount()] = $book;

        return $this->getBookCount();
    }

    public function removeBook(OnTheBookShelf $book): self
    {
        $info = $book->getBookInfo();

        $counter = 0;
        while (++$counter <= $this->getBookCount()) {
            if ($this->books[$counter]->getBookInfo() === $info) {
                for ($x = $counter; $x < $this->getBookCount(); ++$x) {
                    $this->books[$x] = $this->books[$x + 1];
                }

                $this->setBookCount($this->getBookCount() - 1);
            }
        }

        return $this;
    }
}
