<?php

declare(strict_types=1);

namespace App\Structural\Composite;

class OneBook implements OnTheBookShelf
{
    private $title;
    private $author;

    public function __construct(string $title, string $author)
    {
        $this->title = $title;
        $this->author = $author;
    }

    public function getBookInfo(int $index = 1): string
    {
        if (1 === $index) {
            return $this->title.' by '.$this->author;
        }

        return '';
    }

    public function getBookCount(): int
    {
        return 1;
    }

    public function setBookCount(int $newCount): self
    {
        return $this;
    }

    public function addBook(OnTheBookShelf $book): int
    {
        return $this;
    }

    public function removeBook(OnTheBookShelf $book): self
    {
        return $this;
    }
}
