<?php

declare(strict_types=1);

namespace App\Structural\Composite;

interface OnTheBookShelf
{
    public function getBookInfo(int $index = 1): string;

    public function getBookCount(): int;

    public function setBookCount(int $newCount): self;

    public function addBook(self $book): int;

    public function removeBook(self $book): self;
}
